DSY Annotation Manager Plugin
======

Este plugin tiene la finalidad de crear notas de audio o de texto con el fin de enviar dicho texto o url de audio como callback.


Instalación
============

NOTA: (Comentario hecho por Matías) no estoy seguro de que sudo sea siempre necesario, una buena práctica es probar primero sin sudo.

Para poder instalar el plugin se debe colocar el siguiente comando en la terminal:

```sudo cordova plugin add https://bitbucket.org/dsarhoya/cordova-rl-annotations-plugin  --save --nofetch```


Ahora bien, para el caso de agregar la plataforma correspondiente (por ejemplo ios) se debe aplicar lo siguiente en la terminal:

```sudo ionic cordova platform add ios```

Una vez hecho lo siguiente lo unico que resta es otorgar los permisos de escritura en la carpeta de ios:

```sudo chmode -R 777 platforms/ios```

Usabilidad
==========

### Abrir Recurso

Para poder llamar al recurso basta con llamar a la función ```openResource($url_del_recurso,$callback)``` dentro de la plataforma ionic. A modo de ejemplo para ```ionic 1``` dicha función se llamaría de la siguiente manera:

```javascript
  .controller('controlador',function($scope,$ionicPlatform){
    $ionicPlatform.ready(function(){
        DSYAnnotationManagerPlugin.openResource("http://www.dsy.cl",function($message){
          console.log($message);                         
        });
    });
```

Tal como se puede apreciar en el ejemplo anterior, el callback del recurso posee un parámetro llamado ```$message```, este corresponde al mensaje de texto o url de la nota de audio.

### Utilización de Acusos de Recibo

A cada recurso se le puede enviar un *acuso de recibo* por parte de la plataforma. Ya que el Annotation Manager posee los métodos de ```annotationSentOK()```y ```annotationSentFail()``` que corresponden al éxito o a alguna falla. Por lo tanto al llamar a estos métodos la interfaz arroja un mensaje de error o de éxito.

A modo de ejemplo, en ```ionic 1``` se puede utilizar de la siguiente manera:

```javascript
  .controller('controlador',function($scope,$ionicPlatform){
    $ionicPlatform.ready(function(){

        DSYAnnotationManagerPlugin.openResource("http://www.dsy.cl",function($message){

          $ionicPlatform.ready(function(){

            setTimeout(function(){

              DSYAnnotationManagerPlugin.annotationSentOK();

              },3000);
            });                         
        });
    });
```

### Recursos de Prueba

Para el caso de utilizar recursos de prueba, el plugin facilita una url de anotacion de voz de prueba utilizando la clase ```DSYAnnotationManagerPluginTest```. Un ejemplo de ello es utilizar un log que devuelve dicha dirección de prueba:

```javascript
  .controller('controlador',function($scope,$ionicPlatform){
    $ionicPlatform.ready(function(){
          DSYAnnotationManagerPluginTest.getAnnotation(function($url){
            console.log($url);
          });
        });
    });
```

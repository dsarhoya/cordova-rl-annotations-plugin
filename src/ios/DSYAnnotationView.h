//
//  DSYAnnotationView.h
//  MyApp
//
//  Created by Alfredo Luco on 31-10-17.
//

#import <UIKit/UIKit.h>
#import "DSYTextAnnotationView.h"
#import <AVFoundation/AVFoundation.h>

@class DSYAudioManager;
@interface DSYAnnotationView : UIView<UITextFieldDelegate,DSYTextAnnotationViewDelegate>

@property (readwrite,nonatomic) UITextView* bottomTextView;
@property (readwrite,nonatomic) UIButton* bottomButton;
@property (readwrite) BOOL isRecording;
@property (readwrite, nonatomic) DSYAudioManager* audiomgr;

-(void)setupButton:(UIButton*)button;

-(BOOL)hasTextAnnotation;
-(BOOL)hasAudioAnnotation;

-(NSString*)getTextAnnotation;
-(AVURLAsset*)getAudioAnnotation;

@end

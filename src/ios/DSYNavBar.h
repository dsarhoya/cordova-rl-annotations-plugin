//
//  DSYNavBar.h
//  MyApp
//
//  Created by Alfredo Luco on 02-11-17.
//

#import <UIKit/UIKit.h>

@protocol DSYNavBarDelegate<NSObject>

-(void)didDeleteAnnotation;
-(void)didSendAnnotation;

@end

@interface DSYNavBar : UIView

@property (weak,nonatomic) id<DSYNavBarDelegate> delegate;

@end

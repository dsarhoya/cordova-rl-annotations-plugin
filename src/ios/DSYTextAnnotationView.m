//
//  DSYTextAnnotationView.m
//  MyApp
//
//  Created by Alfredo Luco on 31-10-17.
//

#import "DSYTextAnnotationView.h"

@interface DSYTextAnnotationView ()

@property (readwrite,nonatomic) UIButton* closeButton;

@end

@implementation DSYTextAnnotationView

-(instancetype)initWithMessage:(NSString *)message{
    if(!(self = [super init])){
        return nil;
    }
    if(self){
        [self setup:message];
        [self buttonSetup];
    }
    return self;
}

#pragma mark - setup

-(void)setup:(NSString*)message{
    self.backgroundColor = [UIColor colorWithRed:0.667 green:0.667 blue:0.667 alpha:1.0];
    self.message = [[UITextView alloc] init];
    self.message.text = message;
    self.message.editable = NO;
    
    self.closeButton = [[UIButton alloc] init];
    
    [self addSubview:_message];
    [self addSubview:_closeButton];
    [self sendSubviewToBack:_message];
    
    self.closeButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.message.translatesAutoresizingMaskIntoConstraints = NO;
    self.closeButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[message]-|" options:0 metrics:nil views:@{@"message" : _message}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[button(15)]-(16)-|" options:0 metrics:nil views:@{@"button" : _closeButton}]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[message]-|" options:0 metrics:nil views:@{@"message" : _message}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(14)-[button(20)]" options:0 metrics:nil views:@{@"button" : _closeButton}]];
    
}

-(void)buttonSetup{
    [self.closeButton setImage:[UIImage imageNamed:@"icon-trash-grey.png"] forState:UIControlStateNormal];
    self.closeButton.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
    self.closeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [self.closeButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [self.closeButton addTarget:self action:@selector(toggleButton) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - selectors
-(void)toggleButton{
    if(self.anotationDelegate){
        [self.anotationDelegate didDeleteMessage];
    }
}

@end

//
//  DSYAnnotationManagerPluginTest.h
//  MyApp
//
//  Created by Alfredo Luco on 06-11-17.
//

#import <Cordova/CDVPlugin.h>
#import <AVFoundation/AVFoundation.h>

@interface DSYAnnotationManagerPluginTest : CDVPlugin

- (void)getAnnotation: (CDVInvokedUrlCommand *)command;


@end

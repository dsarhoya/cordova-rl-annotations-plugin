//
//  DSYAnnotationManagerPluginTest.m
//  MyApp
//
//  Created by Alfredo Luco on 06-11-17.
//

#import "DSYAnnotationManagerPluginTest.h"
#import <Cordova/CDVAvailability.h>

@implementation DSYAnnotationManagerPluginTest

-(void)getAnnotation:(CDVInvokedUrlCommand *)command{
    
    NSError* error;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"beep" ofType:@"mp3"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *mp3Path = [documentsDirectory stringByAppendingPathComponent:@"beep.mp3"];
    
    [fileManager copyItemAtPath:path toPath:mp3Path error:&error];
    
    NSString* callbackID = command.callbackId;
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:@{@"message" : mp3Path, @"type" : @"audio"}];
    //    CDVPluginResult * pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:mp3Path];
    [pluginResult setKeepCallbackAsBool:YES];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackID];
}


@end

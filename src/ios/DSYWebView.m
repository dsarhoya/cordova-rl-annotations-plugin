//
//  DSYWebView.m
//  MyApp
//
//  Created by Alfredo Luco on 31-10-17.
//

#import "DSYWebView.h"
#import <WebKit/WebKit.h>

@interface DSYWebView ()

@property (readwrite, nonatomic) WKWebView* webView;

@end

@implementation DSYWebView

-(instancetype)initWithUrl:(NSURL *)url{
    if(!(self = [super init]))
        return nil;
    if(self){
        [self setup:url];
    }
    return self;
}

#pragma mark - setup
-(void)setup:(NSURL*)url{
    self.webView = [[WKWebView alloc] init];
    self.webView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.webView.frame = self.bounds;
    self.webView.navigationDelegate = self;
    self.webView.UIDelegate = self;
    NSMutableURLRequest *nsrequest = [NSMutableURLRequest requestWithURL:url];
    [self.webView loadRequest:nsrequest];
    [self addSubview:self.webView];
    [self bringSubviewToFront:self.webView];
}

#pragma mark - web view delegate
- (void)webView:(WKWebView *)webView didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler {
    NSLog(@"Allowing all");
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
    CFDataRef exceptions = SecTrustCopyExceptions (serverTrust);
    SecTrustSetExceptions (serverTrust, exceptions);
    CFRelease (exceptions);
    completionHandler (NSURLSessionAuthChallengeUseCredential, [NSURLCredential credentialForTrust:serverTrust]);
}

-(void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error{
    NSLog(@"error NAVIGATION : %@",error.localizedDescription);
}

-(void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation{
    NSLog(@"loading url");
}

-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    NSLog(@"url loaded");
}

@end

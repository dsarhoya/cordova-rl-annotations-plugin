//
//  DSYNavBar.m
//  MyApp
//
//  Created by Alfredo Luco on 02-11-17.
//

#import "DSYNavBar.h"

@interface DSYNavBar ()

@property (readwrite,nonatomic) UIButton* deleteButton;
@property (readwrite,nonatomic) UIButton* sendButton;

@end

@implementation DSYNavBar

-(instancetype)init{
    if(!(self = [super init]))
        return nil;
    if(self){
        [self setup];
    }
    return self;
}

#pragma mark - setup
-(void)setup{
    UIView* view = [[UIView alloc] init];
    [view setBackgroundColor:[UIColor colorWithRed:0.667 green:0.667 blue:0.667 alpha:1.0]];
    
    self.deleteButton = [[UIButton alloc] init];
    [self.deleteButton setImage:[UIImage imageNamed:@"remove-symbol.png"] forState:UIControlStateNormal];
    [self.deleteButton addTarget:self action:@selector(delete) forControlEvents:UIControlEventTouchUpInside];
    self.deleteButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.deleteButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.deleteButton.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
    
    self.sendButton = [[UIButton alloc] init];
    [self.sendButton setTitle:@"Enviar" forState:UIControlStateNormal];
    [self.sendButton setTitleColor:[UIColor colorWithRed:0.965 green:0.82 blue:0.125 alpha:1.0] forState:UIControlStateNormal];
    [self.sendButton addTarget:self action:@selector(send) forControlEvents:UIControlEventTouchUpInside];
    
    [view addSubview:self.deleteButton];
    [view addSubview:self.sendButton];
    
    self.deleteButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.sendButton.translatesAutoresizingMaskIntoConstraints = NO;
    view.translatesAutoresizingMaskIntoConstraints = NO;
    
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(16)-[delete(20)]" options:0 metrics:nil views:@{@"delete" : self.deleteButton}]];
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[send]-(16)-|" options:0 metrics:nil views:@{@"send" : self.sendButton}]];
    
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(>=5)-[delete(25)]-(>=5)-|" options:0 metrics:nil views:@{@"delete" : self.deleteButton}]];
    [view addConstraint:[NSLayoutConstraint constraintWithItem:self.deleteButton attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:1.0]];
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(>=5)-[send]-(>=5)-|" options:0 metrics:nil views:@{@"send" : self.sendButton}]];
    [view addConstraint:[NSLayoutConstraint constraintWithItem:self.sendButton attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:1.0]];
    
    [self addSubview:view];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[view]-(0)-|" options:0 metrics:nil views:@{@"view" : view}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view" : view}]];
}

#pragma mark - selectors
-(void)delete{
    if(self.delegate){
        [self.delegate didDeleteAnnotation];
    }
}

-(void)send{
    if(self.delegate){
        [self.delegate didSendAnnotation];
    }
}

@end

package cl.dsarhoya.plugins;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

public class ContentViewerActivity extends Activity {

    public static final String PARAM_RESOURCE_URL = "url_resource";

    public static final String ANNOTATION_SENT_ACTION = "annotationSentOK";
    public static final String ANNOTATION_DELIVERY_FAILED_ACTION = "annotationSentFail";

    private String resourceUrl;

    private View annotationInput1;
    private View annotationInput2View;
    private View annotationInput3View;
    ProgressDialog progressDialog;

    private MediaRecorder voiceRecorder;
    private MediaPlayer player;

    private EditText textAnnotationET;

    ToneGenerator tg;

    private View textInput;

    private String lastRecordingFilePath;

    private BroadcastReceiver okReceiver;
    private BroadcastReceiver failedReceiver;

    private WebView contentView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(getApplication().getResources().getIdentifier("activity_content_viewer", "layout", getApplication().getPackageName()));

        resourceUrl = getIntent().getStringExtra(PARAM_RESOURCE_URL);

        contentView = (WebView) getViewById("webview");
        WebSettings settings = contentView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAllowFileAccessFromFileURLs(true);
        settings.setAllowUniversalAccessFromFileURLs(true);
        settings.setBuiltInZoomControls(true);
        loadResource(resourceUrl);

        okReceiver = new AnnotationSentReceiver();
        failedReceiver = new AnnotationDeliveryFailedReceiver();

        registerReceiver(okReceiver, new IntentFilter(ANNOTATION_SENT_ACTION));
        registerReceiver(failedReceiver, new IntentFilter(ANNOTATION_DELIVERY_FAILED_ACTION));

        annotationInput1 = getViewById("annotation_input_1");
        annotationInput2View = getViewById("annotation_input_2");
        annotationInput3View = getViewById("annotation_input_3");
        textInput = getViewById("text_input");
        textAnnotationET = (EditText) getViewById("text");

        annotationInput2View.setVisibility(View.GONE);
        annotationInput3View.setVisibility(View.GONE);

        tg = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);

        Button closeButton = (Button) getViewById("button_close");
        closeButton.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            finish();
          }
        });

        textInput.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            annotationInput1.setVisibility(View.GONE);
            annotationInput2View.setVisibility(View.VISIBLE);
            textAnnotationET.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(textAnnotationET, InputMethodManager.SHOW_IMPLICIT);

          }
        });




      final Button saveButton = (Button) getViewById("button_send");
      saveButton.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {

          if (textPreviewIsVisible() || audioPreviewIsVisible()){

            stopPlaying();

            if (textPreviewIsVisible()){
              String textAnnotation = textAnnotationET.getText().toString();
              DSYAnnotationManagerPlugin.sendAnnotation("text", textAnnotation);
            }else if (audioPreviewIsVisible()){
              DSYAnnotationManagerPlugin.sendAnnotation("audio", lastRecordingFilePath);
            }

            progressDialog = ProgressDialog.show(ContentViewerActivity.this, "", "Enviando...", true, false);

          }

        }
      });

      getViewById("button_clear").setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          clearTextAnnotation();

        }
      });

      getViewById("button_audio_dismiss").setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          clearAudioAnnotation();

        }
      });

      final TextView promptRecordingTV = (TextView) getViewById("prompt_recording");

      getViewById("audio_input").setOnTouchListener(new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
          if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {


            tg.startTone(ToneGenerator.TONE_PROP_BEEP);

            textInput.setVisibility(View.GONE);
            promptRecordingTV.setVisibility(View.VISIBLE);

            startRecording();
            return true;
          } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {

            if (stopRecording()){
              textInput.setVisibility(View.VISIBLE);
              promptRecordingTV.setVisibility(View.GONE);
              annotationInput1.setVisibility(View.GONE);
              annotationInput3View.setVisibility(View.VISIBLE);
            }else{
              textInput.setVisibility(View.VISIBLE);
              promptRecordingTV.setVisibility(View.GONE);
            }

            tg.startTone(ToneGenerator.TONE_PROP_ACK);
            return true;
          }

          return false;
        }
      });

      getViewById("button_audio_play").setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          startPlaying();
        }
      });

      getViewById("button_audio_pause").setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          if (player != null && player.isPlaying()){
            player.pause();
          }

        }
      });
    }

    private void loadResource(String url){

        if (url.contains(".pdf")){

            final ProgressDialog progress = ProgressDialog.show(ContentViewerActivity.this, "", "Obteniendo archivo...", true, false);

            FileDownloader downloader = new FileDownloader(this, resourceUrl, "rl-resource.pdf") {
                @Override
                protected void onFileDownloaded(File file) {
                    contentView.setWebChromeClient(new WebChromeClient());
                    contentView.loadUrl("file:///android_asset/pdfjs/viewer/viewer.html?file=" + file.getAbsolutePath());
                    progress.dismiss();
                }

                @Override
                protected void onDownloadFailed() {
                    Toast.makeText(getApplicationContext(), "No se ha logrado obtener el archivo.", Toast.LENGTH_LONG).show();
                    progress.dismiss();
                    finish();
                }
            };
            downloader.execute();


        }else{
            contentView.setWebViewClient(new WebViewClient());
            contentView.loadUrl(url);
        }

    }

    private void clearAudioAnnotation(){
      annotationInput1.setVisibility(View.VISIBLE);
      annotationInput3View.setVisibility(View.GONE);

      stopPlaying();
    }

    private void clearTextAnnotation(){
      textAnnotationET.setText("");

      InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
      imm.hideSoftInputFromWindow(textAnnotationET.getWindowToken(), 0);

      annotationInput1.setVisibility(View.VISIBLE);
      annotationInput2View.setVisibility(View.GONE);

      lastRecordingFilePath = null;
    }

    private View getViewById(String viewId){
      String packageName = getApplication().getPackageName();
      return findViewById(getResources().getIdentifier(viewId, "id", packageName ));
    }

    private void startRecording() {
      voiceRecorder = new MediaRecorder();

      lastRecordingFilePath = String.format("%s/audio_annotation_%d.3gp",getFilesDir().getAbsolutePath(), System.currentTimeMillis());

      voiceRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
      voiceRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
      voiceRecorder.setOutputFile(lastRecordingFilePath);
      voiceRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

      try {
        voiceRecorder.prepare();
        voiceRecorder.start();
      } catch (IOException e) {
        Log.e(getClass().getSimpleName(), "prepare() failed", e);
      }


    }

  private boolean stopRecording() {

    try{
      voiceRecorder.stop();
      voiceRecorder.release();
      voiceRecorder = null;

      return true;
    }catch (RuntimeException ex){
      return false;
    }

  }

  private void startPlaying() {

    if (player != null && !player.isPlaying() && player.getCurrentPosition() != 0 ){
      player.start();
    }else{
      player = new MediaPlayer();
      try {
        player.setDataSource(lastRecordingFilePath);
        player.prepare();
        player.start();
      } catch (IOException e) {
        Log.e(getClass().getSimpleName(), "prepare() failed");
      }
    }
  }


  private void stopPlaying(){

    if (player != null){
      player.stop();
      player.release();
      player = null;
    }

  }

  @Override
    protected void onDestroy() {
      super.onDestroy();
      tg.release();

      stopPlaying();

      unregisterReceiver(okReceiver);
      unregisterReceiver(failedReceiver);
    }


    private boolean audioPreviewIsVisible(){
      return annotationInput3View.getVisibility() == View.VISIBLE;
    }

    private boolean textPreviewIsVisible(){
      return annotationInput2View.getVisibility() == View.VISIBLE;
    }

    public class AnnotationSentReceiver extends BroadcastReceiver{

      public AnnotationSentReceiver() {
      }

      @Override
      public void onReceive(Context context, Intent intent) {

        if (progressDialog != null){
          progressDialog.dismiss();
          progressDialog = null;

          if (textPreviewIsVisible()){
            clearTextAnnotation();
          }

          if (audioPreviewIsVisible()){
            clearAudioAnnotation();
          }
        }

      }
    }

    class AnnotationDeliveryFailedReceiver  extends BroadcastReceiver{
      @Override
      public void onReceive(Context context, Intent intent) {

        if (progressDialog != null){
          progressDialog.dismiss();
          progressDialog = null;
          Toast.makeText(ContentViewerActivity.this, "Envío fallido. Intente de nuevo.", Toast.LENGTH_SHORT).show();
        }

      }
    }
}
